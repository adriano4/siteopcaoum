<?php
# Replace text/html with whatever MIME-type you prefer.
header("Content-Type: text/html; charset=utf-8");

// Check for empty fields
if(empty($_POST['name'])      ||
   empty($_POST['email'])     ||
   empty($_POST['phone'])     ||
   empty($_POST['message'])   ||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
   echo "No arguments Provided!";
   return false;
   }
   
$name = strip_tags(htmlspecialchars($_POST['name']));
$email_address = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = strip_tags(htmlspecialchars($_POST['message']));
   
// Crie o email e envie a mensagem
$to = 'contato@opcaoum.com.br'; // Adicione seu endereço de e-mail entre o '' substituindo yourname@seudominio.com - Este é o lugar para onde o formulário enviará uma mensagem.
$email_subject = "Formulário de contato do site:  $name";
$email_body = "Você recebeu uma nova mensagem do formulário de contato do seu website.\n\n"."Aqui estão os detalhes:\n\nNome: $name\n\nEmail: $email_address\n\nTelefone: $phone\n\nMensagem:\n$message";
$headers = "From: amoreno@opcaoum.com.br\n"; // Este é o endereço de e-mail da mensagem gerada. Recomendamos usar algo como noreply@yourdomain.com
$headers .= "Reply-To: $email_address";   
mail($to,$email_subject,$email_body,$headers);
return true;    
?>
     